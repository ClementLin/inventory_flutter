import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/component/login.dart';
import 'package:flutter_app/home/home_component.dart';

var rootHandler = new Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
      return new LoginComponent();
    });

var mainHandler = new Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
      return new MainComponent();
    });