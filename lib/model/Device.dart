
class Device{
  final int id;
  final String owner;
  final String productNum;
  final int active;
  final String brandName;
  final String kindName;
  final String itemName;

  Device(this.id, this.owner, this.productNum, this.active, this.brandName, this.itemName, this.kindName);

  factory Device.fromJson(Map<String, dynamic> parsedJson){
    return Device(
      parsedJson['id'],
      parsedJson['owner'],
      parsedJson['productNum'],
      parsedJson['active'],
      parsedJson['brandName'],
      parsedJson['itemName'],
      parsedJson['kindName']);
  }
}