
import 'package:fluro/fluro.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/route_config/Application.dart';

class LoginComponent extends StatefulWidget {
  @override
  LoginComponentState createState() => LoginComponentState();
}

class LoginComponentState extends State<LoginComponent>{

  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: ListView(
            padding: const EdgeInsets.symmetric(horizontal: 24.0),
            children: <Widget>[
              const SizedBox(height: 120.0),
              Column(
                children: <Widget>[
                  Image.asset(
                      'res/app_icon.png',
                    width: 60.0,
                    height: 60.0,
                  ),
                  SizedBox(height: 16.0),
                  Text(
                    "INVENTORY",
                    style: Theme.of(context).textTheme.headline,
                  ),
                ],
              ),
              const SizedBox(height: 100.0),
              TextField(
                controller: _usernameController,
                decoration: const InputDecoration(
                  labelText: 'Username',
                ),
              ),
              const SizedBox(height: 12.0),
              TextField(
                controller: _passwordController,
                decoration: const InputDecoration(
                  labelText: 'Password',
                ),
              ),
              const SizedBox(height: 12.0),
              Wrap(
                children: <Widget>[
                  ButtonBar(
                    children: <Widget>[
                      FlatButton(
                        child: const Text('CANCEL'),
                        shape: const BeveledRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(7.0)),
                        ),
                        onPressed: () {

                        },
                      ),
                      RaisedButton(
                        child: const Text('NEXT'),
                        elevation: 8.0,
                        shape: const BeveledRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(7.0)),
                        ),
                        onPressed: () {
                          Application.router.navigateTo(context, "/test", transition: TransitionType.fadeIn);
                        },
                      ),
                    ],
                  )
                ],
              ),
            ],
          )
      )
    );
  }

}
