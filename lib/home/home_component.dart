import 'dart:async' show Future;
import 'dart:convert';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/model/Device.dart';
import 'package:flutter_app/widget/InventoryList.dart';
import 'package:flutter_app/widget/MainTitle.dart';
import 'package:flutter_app/widget/SquareBox.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/services.dart' show rootBundle;

class MainComponent extends StatefulWidget {
  @override
  MainComponentState createState() => MainComponentState();
}

class MainComponentState extends State<MainComponent>{


  int currentTabIndex = 0;
  String result = "Hey there !";
  int itemCount = 0;
  List<String> litems = ["555", "529", "486", "522"];
  List<String> scanedItems = [];

//  var device = Device()
  Future _scanQR() async {
    String qrResult = await BarcodeScanner.scan();
    setState(() {
      result = qrResult;
      scanedItems.add(qrResult);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
//        appBar: AppBar(
//          leading: IconButton(icon: Icon(Icons.menu), onPressed: (){}),
//            title: Text('盤起來'),
//
//        ),

        body: SafeArea(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                const SizedBox(height: 40.0),
                MainTitle(),
                const SizedBox(height: 30.0),
                SquareBox(),
                const SizedBox(height: 30.0),
                InventoryList()
              ],
            ),
        ),

        floatingActionButton: FloatingActionButton(
          onPressed: (){
            _scanQR();
          },
          child: Image.asset('res/ic_qr_code.png', width: 30.0, height: 30.0,),
          backgroundColor: Colors.lightBlue,
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,

        bottomNavigationBar: BottomAppBar(
          child: new Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              IconButton(icon: Icon(Icons.menu), onPressed: () {},),
              IconButton(icon: Icon(Icons.people), onPressed: () {},),
              const Expanded(child: SizedBox()),
              IconButton(icon: Icon(Icons.refresh), onPressed: () {
                setState(() {
                  scanedItems.clear();
                });
              },),
            ],
          ),
          shape: CircularNotchedRectangle(),
        )
    );
  }

  _buildRow(int index){
    return Card(
      color: Colors.white,
      margin: EdgeInsets.all(5.0),
      child: ListTile(
          leading: _isChecked(index),
          title: GestureDetector(
            onTap: (){
              Fluttertoast.showToast(
                  msg: 'Item: ${litems[index]}',
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.CENTER,
                  timeInSecForIos: 1,
                  backgroundColor: Colors.red,
                  textColor: Colors.white,
                  fontSize: 12.0
              );
            },
            child: Text('Item: ${litems[index]}'),
          ),
          trailing: Icon(Icons.arrow_forward_ios)
      ),
    );
  }

  _buildRow2(Device device){
    return Card(
      color: Colors.white,
      margin: EdgeInsets.all(5.0),
      child: ListTile(
          leading: Icon(Icons.check_box_outline_blank),
          title: GestureDetector(
            onTap: (){
            },
            child: Text('No.${device.id}: ${device.itemName}'),
          ),
          trailing: Icon(Icons.arrow_forward_ios)
      ),
    );
  }

  _isChecked(int index) {
    if(scanedItems.contains(litems[index]))
      return Icon(Icons.check_circle);
    else
      return Icon(Icons.check_box_outline_blank);
  }

  _getSid() async {
    var url = "https://35.236.139.149/elibomApi/WebService/Inventory/Manager/Login";
    var header = {'Ekey':'yrotnevni'};
    http.put(
        url,
        headers: header,
        body: { "account":"clement_lin", "password":"12345678"})
        .then((response) {
        print("Response status: ${response.statusCode}");
        print("Response body: ${response.body}");
    });
  }

  Future<String> _getFileData(String path) async {
    return await rootBundle.loadString(path);
  }

//  Future<String> _loadDevice() async {
//    String deviceData = await _getFileData('assets/res/devices.json');
//    final jsonResult =
//    return await rootBundle.loadString(path);
//  }

}