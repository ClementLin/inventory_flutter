
import 'package:flutter/material.dart';

class MainTitle extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
      return Stack(
        children: <Widget>[
          Center(
            child:
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Inventory",
                  style: Theme.of(context).textTheme.headline,
                ),
                const SizedBox(height: 10.0),
                Text(
                  "_Name",
                  style: Theme.of(context).textTheme.subtitle,
                )
              ],
            ),
          ),
          Align(
            alignment: Alignment(0.9, 0),
            child: IconButton(icon: Icon(Icons.search), onPressed: (){}),
          )
        ],
      );
  }
}