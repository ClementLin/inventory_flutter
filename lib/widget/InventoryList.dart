
import 'package:flutter/material.dart';
import 'dart:async' show Future;
import 'dart:convert';
import 'package:flutter/services.dart' show rootBundle;
import 'package:flutter_app/model/Device.dart';

class InventoryList extends StatefulWidget{

  @override
  InventoryListState createState() => InventoryListState();
}

class InventoryListState extends State<InventoryList>{


  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
          future: DefaultAssetBundle.of(context).loadString('res/devices.json'),
          builder: (context, snapshot){
            print("data = ${snapshot.data.toString()}");
            var newData = json.decode(snapshot.data.toString());
            if (newData == null){
              return Center(
                child: CircularProgressIndicator(),
              );
            } else {
              var datas = newData['data'] as List;
              var list = datas.map<Device>((json) => Device.fromJson(json)).toList();
              print("datas len = ${datas.length}");
              return Expanded(
                child: ListView.builder(
                    itemCount: newData == null ? 0:datas.length,
                    itemBuilder: (context, index) => _buildItem(list[index])),
              );
            }
          },
        );
  }

  _buildItem(Device device){
    return Card(
      color: Colors.white,
      margin: EdgeInsets.all(5.0),
      child: ListTile(
          leading: Icon(Icons.check_box_outline_blank),
          title: GestureDetector(
            onTap: (){
            },
            child: Text('No.${device.id}: ${device.itemName}'),
          ),
          trailing: Icon(Icons.arrow_forward_ios)
      ),
    );
  }
}