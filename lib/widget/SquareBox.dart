
import 'package:flutter/material.dart';

class SquareBox extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
      return Row(
          children: <Widget>[
            Expanded(
                child: Center(
                    child: Column(
                      children: <Widget>[
                        Text(
                          "_item",
                          style: Theme.of(context).textTheme.subtitle,
                        ),
                        Text(
                          "Total Item",
                          style: Theme.of(context).textTheme.subtitle,
                        )
                      ],
                    )
                )
            ),
            Expanded(
                child: Center(
                  child:Column(
                    children: <Widget>[
                      Text(
                        "_item",
                        style: Theme.of(context).textTheme.subtitle,
                      ),
                      Text(
                        "Total Item",
                        style: Theme.of(context).textTheme.subtitle,
                      )
                    ],
                  ),
                )
            )
          ]
      );
  }
}